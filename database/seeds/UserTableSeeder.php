<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_manager  = Role::where('name', 'manager')->first();
        
        $employee = new User();
        $employee->name = 'CEO';
        $employee->email = 'ceo@example.com';
        $employee->password = bcrypt('secret');
        $employee->save();
        $employee->roles()->attach($role_manager);
    }
}
