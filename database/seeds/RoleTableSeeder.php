<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = 'employee';
        $role_employee->description = 'A employee user';
        $role_employee->save();
        
        $role_manager = new Role();
        $role_manager->name = 'manager';
        $role_manager->description = 'A manager user';
        $role_manager->save();
    }
}
