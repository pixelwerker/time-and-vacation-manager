<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Ermittelt alle Rollen dieses Benutzers.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    
    /**
     * Überprüft, ob der Benutzer über die zu überprüfenden Rollen verfügt.
     *
     * @param string|array $roles
     * @return bool
     * 
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) || abort(401, 'This action is unauthorized.');
        }
        
        return $this->hasRole($roles) || abort(401, 'This action is unauthorized.');
    }
    
    /**
     * Überprüft mehrere Rollen.
     *
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    
    /**
     * Überprüft eine Rolle.
     * 
     * @param string $role
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }
}
