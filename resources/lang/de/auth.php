<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Leider waren die Benutzerdaten nicht korrekt.',
    'throttle' => 'Sie benötigen zu viele Anmeldeversuche. Probieren Sie es in :seconds Sekunden erneut.',

];
