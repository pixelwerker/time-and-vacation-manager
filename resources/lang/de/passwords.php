<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Das Passwort muss eine Mindestlänge von sechs Zeichen haben und mit der Passwort-Bestätigung übereinstimmen.',
    'reset' => 'Ihr Passwort wurde zurückgesetzt.',
    'sent' => 'Ihnen wurde eine E-Mail mit dem Link zum Zurücksetzen zugesendet.',
    'token' => 'Leider ist der aktuell verwendete Link zum Zurücksetzen bereits ungültig. Vordern Sie einen neuen an.',
    'user' => 'Es konnte kein Benutzer zu dieser E-Mail-Adresse ermittelt werden.',

];
